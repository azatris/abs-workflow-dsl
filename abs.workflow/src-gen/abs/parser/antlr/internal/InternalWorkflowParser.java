package abs.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import abs.services.WorkflowGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWorkflowParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'process:'", "'model:'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalWorkflowParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWorkflowParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWorkflowParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWorkflow.g"; }



     	private WorkflowGrammarAccess grammarAccess;

        public InternalWorkflowParser(TokenStream input, WorkflowGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StateMachine";
       	}

       	@Override
       	protected WorkflowGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStateMachine"
    // InternalWorkflow.g:64:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalWorkflow.g:64:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalWorkflow.g:65:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalWorkflow.g:71:1: ruleStateMachine returns [EObject current=null] : ( (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) ) (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ ) ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_startState_3_0 = null;

        EObject lv_intermediateStates_4_0 = null;

        EObject lv_endStates_5_0 = null;



        	enterRule();

        try {
            // InternalWorkflow.g:77:2: ( ( (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) ) (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ ) ) )
            // InternalWorkflow.g:78:2: ( (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) ) (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ ) )
            {
            // InternalWorkflow.g:78:2: ( (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) ) (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ ) )
            // InternalWorkflow.g:79:3: (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) ) (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ )
            {
            // InternalWorkflow.g:79:3: (otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalWorkflow.g:80:4: otherlv_0= 'process:' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            				newLeafNode(otherlv_0, grammarAccess.getStateMachineAccess().getProcessKeyword_0_0());
            			
            // InternalWorkflow.g:84:4: ( (lv_name_1_0= RULE_ID ) )
            // InternalWorkflow.g:85:5: (lv_name_1_0= RULE_ID )
            {
            // InternalWorkflow.g:85:5: (lv_name_1_0= RULE_ID )
            // InternalWorkflow.g:86:6: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            						newLeafNode(lv_name_1_0, grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_1_0());
            					

            						if (current==null) {
            							current = createModelElement(grammarAccess.getStateMachineRule());
            						}
            						setWithLastConsumed(
            							current,
            							"name",
            							lv_name_1_0,
            							"org.eclipse.xtext.common.Terminals.ID");
            					

            }


            }


            }

            // InternalWorkflow.g:103:3: (otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+ )
            // InternalWorkflow.g:104:4: otherlv_2= 'model:' ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )? ( (lv_endStates_5_0= ruleEndState ) )+
            {
            otherlv_2=(Token)match(input,12,FOLLOW_3); 

            				newLeafNode(otherlv_2, grammarAccess.getStateMachineAccess().getModelKeyword_1_0());
            			
            // InternalWorkflow.g:108:4: ( ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )* )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==RULE_ID) ) {
                    int LA2_3 = input.LA(3);

                    if ( (LA2_3==13) ) {
                        alt2=1;
                    }
                }
            }
            switch (alt2) {
                case 1 :
                    // InternalWorkflow.g:109:5: ( (lv_startState_3_0= ruleStartState ) ) ( (lv_intermediateStates_4_0= ruleIntermediateState ) )*
                    {
                    // InternalWorkflow.g:109:5: ( (lv_startState_3_0= ruleStartState ) )
                    // InternalWorkflow.g:110:6: (lv_startState_3_0= ruleStartState )
                    {
                    // InternalWorkflow.g:110:6: (lv_startState_3_0= ruleStartState )
                    // InternalWorkflow.g:111:7: lv_startState_3_0= ruleStartState
                    {

                    							newCompositeNode(grammarAccess.getStateMachineAccess().getStartStateStartStateParserRuleCall_1_1_0_0());
                    						
                    pushFollow(FOLLOW_3);
                    lv_startState_3_0=ruleStartState();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getStateMachineRule());
                    							}
                    							set(
                    								current,
                    								"startState",
                    								lv_startState_3_0,
                    								"abs.Workflow.StartState");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalWorkflow.g:128:5: ( (lv_intermediateStates_4_0= ruleIntermediateState ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==RULE_ID) ) {
                            int LA1_1 = input.LA(2);

                            if ( (LA1_1==RULE_ID) ) {
                                int LA1_3 = input.LA(3);

                                if ( (LA1_3==13) ) {
                                    alt1=1;
                                }


                            }


                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalWorkflow.g:129:6: (lv_intermediateStates_4_0= ruleIntermediateState )
                    	    {
                    	    // InternalWorkflow.g:129:6: (lv_intermediateStates_4_0= ruleIntermediateState )
                    	    // InternalWorkflow.g:130:7: lv_intermediateStates_4_0= ruleIntermediateState
                    	    {

                    	    							newCompositeNode(grammarAccess.getStateMachineAccess().getIntermediateStatesIntermediateStateParserRuleCall_1_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_3);
                    	    lv_intermediateStates_4_0=ruleIntermediateState();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getStateMachineRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"intermediateStates",
                    	    								lv_intermediateStates_4_0,
                    	    								"abs.Workflow.IntermediateState");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalWorkflow.g:148:4: ( (lv_endStates_5_0= ruleEndState ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalWorkflow.g:149:5: (lv_endStates_5_0= ruleEndState )
            	    {
            	    // InternalWorkflow.g:149:5: (lv_endStates_5_0= ruleEndState )
            	    // InternalWorkflow.g:150:6: lv_endStates_5_0= ruleEndState
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getEndStatesEndStateParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_endStates_5_0=ruleEndState();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"endStates",
            	    							lv_endStates_5_0,
            	    							"abs.Workflow.EndState");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleStartState"
    // InternalWorkflow.g:172:1: entryRuleStartState returns [EObject current=null] : iv_ruleStartState= ruleStartState EOF ;
    public final EObject entryRuleStartState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStartState = null;


        try {
            // InternalWorkflow.g:172:51: (iv_ruleStartState= ruleStartState EOF )
            // InternalWorkflow.g:173:2: iv_ruleStartState= ruleStartState EOF
            {
             newCompositeNode(grammarAccess.getStartStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStartState=ruleStartState();

            state._fsp--;

             current =iv_ruleStartState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStartState"


    // $ANTLR start "ruleStartState"
    // InternalWorkflow.g:179:1: ruleStartState returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ ) ;
    public final EObject ruleStartState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_actions_1_0 = null;



        	enterRule();

        try {
            // InternalWorkflow.g:185:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ ) )
            // InternalWorkflow.g:186:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ )
            {
            // InternalWorkflow.g:186:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ )
            // InternalWorkflow.g:187:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+
            {
            // InternalWorkflow.g:187:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalWorkflow.g:188:4: (lv_name_0_0= RULE_ID )
            {
            // InternalWorkflow.g:188:4: (lv_name_0_0= RULE_ID )
            // InternalWorkflow.g:189:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getStartStateAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStartStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalWorkflow.g:205:3: ( (lv_actions_1_0= ruleAction ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    int LA4_1 = input.LA(2);

                    if ( (LA4_1==13) ) {
                        alt4=1;
                    }


                }


                switch (alt4) {
            	case 1 :
            	    // InternalWorkflow.g:206:4: (lv_actions_1_0= ruleAction )
            	    {
            	    // InternalWorkflow.g:206:4: (lv_actions_1_0= ruleAction )
            	    // InternalWorkflow.g:207:5: lv_actions_1_0= ruleAction
            	    {

            	    					newCompositeNode(grammarAccess.getStartStateAccess().getActionsActionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_actions_1_0=ruleAction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStartStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"actions",
            	    						lv_actions_1_0,
            	    						"abs.Workflow.Action");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStartState"


    // $ANTLR start "entryRuleIntermediateState"
    // InternalWorkflow.g:228:1: entryRuleIntermediateState returns [EObject current=null] : iv_ruleIntermediateState= ruleIntermediateState EOF ;
    public final EObject entryRuleIntermediateState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntermediateState = null;


        try {
            // InternalWorkflow.g:228:58: (iv_ruleIntermediateState= ruleIntermediateState EOF )
            // InternalWorkflow.g:229:2: iv_ruleIntermediateState= ruleIntermediateState EOF
            {
             newCompositeNode(grammarAccess.getIntermediateStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntermediateState=ruleIntermediateState();

            state._fsp--;

             current =iv_ruleIntermediateState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntermediateState"


    // $ANTLR start "ruleIntermediateState"
    // InternalWorkflow.g:235:1: ruleIntermediateState returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ ) ;
    public final EObject ruleIntermediateState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_actions_1_0 = null;



        	enterRule();

        try {
            // InternalWorkflow.g:241:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ ) )
            // InternalWorkflow.g:242:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ )
            {
            // InternalWorkflow.g:242:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+ )
            // InternalWorkflow.g:243:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_actions_1_0= ruleAction ) )+
            {
            // InternalWorkflow.g:243:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalWorkflow.g:244:4: (lv_name_0_0= RULE_ID )
            {
            // InternalWorkflow.g:244:4: (lv_name_0_0= RULE_ID )
            // InternalWorkflow.g:245:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_name_0_0, grammarAccess.getIntermediateStateAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIntermediateStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalWorkflow.g:261:3: ( (lv_actions_1_0= ruleAction ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    int LA5_1 = input.LA(2);

                    if ( (LA5_1==13) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // InternalWorkflow.g:262:4: (lv_actions_1_0= ruleAction )
            	    {
            	    // InternalWorkflow.g:262:4: (lv_actions_1_0= ruleAction )
            	    // InternalWorkflow.g:263:5: lv_actions_1_0= ruleAction
            	    {

            	    					newCompositeNode(grammarAccess.getIntermediateStateAccess().getActionsActionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_actions_1_0=ruleAction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getIntermediateStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"actions",
            	    						lv_actions_1_0,
            	    						"abs.Workflow.Action");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntermediateState"


    // $ANTLR start "entryRuleEndState"
    // InternalWorkflow.g:284:1: entryRuleEndState returns [EObject current=null] : iv_ruleEndState= ruleEndState EOF ;
    public final EObject entryRuleEndState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEndState = null;


        try {
            // InternalWorkflow.g:284:49: (iv_ruleEndState= ruleEndState EOF )
            // InternalWorkflow.g:285:2: iv_ruleEndState= ruleEndState EOF
            {
             newCompositeNode(grammarAccess.getEndStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEndState=ruleEndState();

            state._fsp--;

             current =iv_ruleEndState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEndState"


    // $ANTLR start "ruleEndState"
    // InternalWorkflow.g:291:1: ruleEndState returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleEndState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalWorkflow.g:297:2: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalWorkflow.g:298:2: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalWorkflow.g:298:2: ( (lv_name_0_0= RULE_ID ) )
            // InternalWorkflow.g:299:3: (lv_name_0_0= RULE_ID )
            {
            // InternalWorkflow.g:299:3: (lv_name_0_0= RULE_ID )
            // InternalWorkflow.g:300:4: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_name_0_0, grammarAccess.getEndStateAccess().getNameIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getEndStateRule());
            				}
            				setWithLastConsumed(
            					current,
            					"name",
            					lv_name_0_0,
            					"org.eclipse.xtext.common.Terminals.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEndState"


    // $ANTLR start "entryRuleAction"
    // InternalWorkflow.g:319:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalWorkflow.g:319:47: (iv_ruleAction= ruleAction EOF )
            // InternalWorkflow.g:320:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalWorkflow.g:326:1: ruleAction returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalWorkflow.g:332:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalWorkflow.g:333:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalWorkflow.g:333:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) )
            // InternalWorkflow.g:334:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) )
            {
            // InternalWorkflow.g:334:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalWorkflow.g:335:4: (lv_name_0_0= RULE_ID )
            {
            // InternalWorkflow.g:335:4: (lv_name_0_0= RULE_ID )
            // InternalWorkflow.g:336:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_0_0, grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getActionAccess().getEqualsSignGreaterThanSignKeyword_1());
            		
            // InternalWorkflow.g:356:3: ( (otherlv_2= RULE_ID ) )
            // InternalWorkflow.g:357:4: (otherlv_2= RULE_ID )
            {
            // InternalWorkflow.g:357:4: (otherlv_2= RULE_ID )
            // InternalWorkflow.g:358:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActionRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getActionAccess().getDestinationStateDestinationStateCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});

}