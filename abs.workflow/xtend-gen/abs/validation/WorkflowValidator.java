package abs.validation;

import abs.validation.AbstractWorkflowValidator;
import abs.workflow.Action;
import abs.workflow.DestinationState;
import abs.workflow.EndState;
import abs.workflow.IntermediateState;
import abs.workflow.StartState;
import abs.workflow.StateMachine;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class WorkflowValidator extends AbstractWorkflowValidator {
  @Check
  public void checkAllNonStartStatesReachable(final StateMachine sm) {
    StartState _startState = sm.getStartState();
    EList<Action> _actions = _startState.getActions();
    final Function1<Action, String> _function = (Action a) -> {
      DestinationState _destinationState = a.getDestinationState();
      return _destinationState.getName();
    };
    final List<String> startStateDestinationStates = ListExtensions.<Action, String>map(_actions, _function);
    EList<IntermediateState> _intermediateStates = sm.getIntermediateStates();
    final Function1<IntermediateState, List<String>> _function_1 = (IntermediateState s) -> {
      EList<Action> _actions_1 = s.getActions();
      final Function1<Action, String> _function_2 = (Action a) -> {
        DestinationState _destinationState = a.getDestinationState();
        return _destinationState.getName();
      };
      return ListExtensions.<Action, String>map(_actions_1, _function_2);
    };
    List<List<String>> _map = ListExtensions.<IntermediateState, List<String>>map(_intermediateStates, _function_1);
    Iterable<String> _flatten = Iterables.<String>concat(_map);
    final List<String> intermediateStateDestinationStates = IterableExtensions.<String>toList(_flatten);
    EList<IntermediateState> _intermediateStates_1 = sm.getIntermediateStates();
    final Function1<IntermediateState, Boolean> _function_2 = (IntermediateState s) -> {
      return Boolean.valueOf(((!startStateDestinationStates.contains(s.getName())) && (!intermediateStateDestinationStates.contains(s.getName()))));
    };
    final IntermediateState unreachableIntermediateState = IterableExtensions.<IntermediateState>findFirst(_intermediateStates_1, _function_2);
    boolean _notEquals = (!Objects.equal(unreachableIntermediateState, null));
    if (_notEquals) {
      EStructuralFeature _eContainingFeature = unreachableIntermediateState.eContainingFeature();
      this.error("State unreachable from other states.", _eContainingFeature);
    }
    EList<EndState> _endStates = sm.getEndStates();
    final Function1<EndState, Boolean> _function_3 = (EndState s) -> {
      return Boolean.valueOf(((!startStateDestinationStates.contains(s.getName())) && (!intermediateStateDestinationStates.contains(s.getName()))));
    };
    final EndState unreachableEndState = IterableExtensions.<EndState>findFirst(_endStates, _function_3);
    boolean _notEquals_1 = (!Objects.equal(unreachableEndState, null));
    if (_notEquals_1) {
      EStructuralFeature _eContainingFeature_1 = unreachableEndState.eContainingFeature();
      this.error("State unreachable from other states", _eContainingFeature_1);
    }
  }
}
