package abs.validation

import abs.workflow.StateMachine
import org.eclipse.xtext.validation.Check

class WorkflowValidator extends AbstractWorkflowValidator {
	@Check
	def checkAllNonStartStatesReachable(StateMachine sm) {
		val startStateDestinationStates = sm.startState.actions.map[a | a.destinationState.name]
		val intermediateStateDestinationStates = sm.intermediateStates.map[s | s.actions.map[a | a.destinationState.name]].flatten.toList
		
		val unreachableIntermediateState = sm.intermediateStates.findFirst[s | !startStateDestinationStates.contains(s.name) && !intermediateStateDestinationStates.contains(s.name)];
		if (unreachableIntermediateState != null) {
			error("State unreachable from other states.", unreachableIntermediateState.eContainingFeature)
		}
		
		val unreachableEndState = sm.endStates.findFirst[s | !startStateDestinationStates.contains(s.name) && !intermediateStateDestinationStates.contains(s.name)];
		if (unreachableEndState != null) {
			error("State unreachable from other states", unreachableEndState.eContainingFeature)
		}
	}
}
