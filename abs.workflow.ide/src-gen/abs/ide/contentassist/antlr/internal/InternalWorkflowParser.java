package abs.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import abs.services.WorkflowGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWorkflowParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'process:'", "'model:'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalWorkflowParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWorkflowParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWorkflowParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWorkflow.g"; }


    	private WorkflowGrammarAccess grammarAccess;

    	public void setGrammarAccess(WorkflowGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStateMachine"
    // InternalWorkflow.g:53:1: entryRuleStateMachine : ruleStateMachine EOF ;
    public final void entryRuleStateMachine() throws RecognitionException {
        try {
            // InternalWorkflow.g:54:1: ( ruleStateMachine EOF )
            // InternalWorkflow.g:55:1: ruleStateMachine EOF
            {
             before(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getStateMachineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalWorkflow.g:62:1: ruleStateMachine : ( ( rule__StateMachine__Group__0 ) ) ;
    public final void ruleStateMachine() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:66:2: ( ( ( rule__StateMachine__Group__0 ) ) )
            // InternalWorkflow.g:67:2: ( ( rule__StateMachine__Group__0 ) )
            {
            // InternalWorkflow.g:67:2: ( ( rule__StateMachine__Group__0 ) )
            // InternalWorkflow.g:68:3: ( rule__StateMachine__Group__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup()); 
            // InternalWorkflow.g:69:3: ( rule__StateMachine__Group__0 )
            // InternalWorkflow.g:69:4: rule__StateMachine__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleStartState"
    // InternalWorkflow.g:78:1: entryRuleStartState : ruleStartState EOF ;
    public final void entryRuleStartState() throws RecognitionException {
        try {
            // InternalWorkflow.g:79:1: ( ruleStartState EOF )
            // InternalWorkflow.g:80:1: ruleStartState EOF
            {
             before(grammarAccess.getStartStateRule()); 
            pushFollow(FOLLOW_1);
            ruleStartState();

            state._fsp--;

             after(grammarAccess.getStartStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStartState"


    // $ANTLR start "ruleStartState"
    // InternalWorkflow.g:87:1: ruleStartState : ( ( rule__StartState__Group__0 ) ) ;
    public final void ruleStartState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:91:2: ( ( ( rule__StartState__Group__0 ) ) )
            // InternalWorkflow.g:92:2: ( ( rule__StartState__Group__0 ) )
            {
            // InternalWorkflow.g:92:2: ( ( rule__StartState__Group__0 ) )
            // InternalWorkflow.g:93:3: ( rule__StartState__Group__0 )
            {
             before(grammarAccess.getStartStateAccess().getGroup()); 
            // InternalWorkflow.g:94:3: ( rule__StartState__Group__0 )
            // InternalWorkflow.g:94:4: rule__StartState__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StartState__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStartStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStartState"


    // $ANTLR start "entryRuleIntermediateState"
    // InternalWorkflow.g:103:1: entryRuleIntermediateState : ruleIntermediateState EOF ;
    public final void entryRuleIntermediateState() throws RecognitionException {
        try {
            // InternalWorkflow.g:104:1: ( ruleIntermediateState EOF )
            // InternalWorkflow.g:105:1: ruleIntermediateState EOF
            {
             before(grammarAccess.getIntermediateStateRule()); 
            pushFollow(FOLLOW_1);
            ruleIntermediateState();

            state._fsp--;

             after(grammarAccess.getIntermediateStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntermediateState"


    // $ANTLR start "ruleIntermediateState"
    // InternalWorkflow.g:112:1: ruleIntermediateState : ( ( rule__IntermediateState__Group__0 ) ) ;
    public final void ruleIntermediateState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:116:2: ( ( ( rule__IntermediateState__Group__0 ) ) )
            // InternalWorkflow.g:117:2: ( ( rule__IntermediateState__Group__0 ) )
            {
            // InternalWorkflow.g:117:2: ( ( rule__IntermediateState__Group__0 ) )
            // InternalWorkflow.g:118:3: ( rule__IntermediateState__Group__0 )
            {
             before(grammarAccess.getIntermediateStateAccess().getGroup()); 
            // InternalWorkflow.g:119:3: ( rule__IntermediateState__Group__0 )
            // InternalWorkflow.g:119:4: rule__IntermediateState__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IntermediateState__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntermediateStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntermediateState"


    // $ANTLR start "entryRuleEndState"
    // InternalWorkflow.g:128:1: entryRuleEndState : ruleEndState EOF ;
    public final void entryRuleEndState() throws RecognitionException {
        try {
            // InternalWorkflow.g:129:1: ( ruleEndState EOF )
            // InternalWorkflow.g:130:1: ruleEndState EOF
            {
             before(grammarAccess.getEndStateRule()); 
            pushFollow(FOLLOW_1);
            ruleEndState();

            state._fsp--;

             after(grammarAccess.getEndStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEndState"


    // $ANTLR start "ruleEndState"
    // InternalWorkflow.g:137:1: ruleEndState : ( ( rule__EndState__NameAssignment ) ) ;
    public final void ruleEndState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:141:2: ( ( ( rule__EndState__NameAssignment ) ) )
            // InternalWorkflow.g:142:2: ( ( rule__EndState__NameAssignment ) )
            {
            // InternalWorkflow.g:142:2: ( ( rule__EndState__NameAssignment ) )
            // InternalWorkflow.g:143:3: ( rule__EndState__NameAssignment )
            {
             before(grammarAccess.getEndStateAccess().getNameAssignment()); 
            // InternalWorkflow.g:144:3: ( rule__EndState__NameAssignment )
            // InternalWorkflow.g:144:4: rule__EndState__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__EndState__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getEndStateAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEndState"


    // $ANTLR start "entryRuleAction"
    // InternalWorkflow.g:153:1: entryRuleAction : ruleAction EOF ;
    public final void entryRuleAction() throws RecognitionException {
        try {
            // InternalWorkflow.g:154:1: ( ruleAction EOF )
            // InternalWorkflow.g:155:1: ruleAction EOF
            {
             before(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getActionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalWorkflow.g:162:1: ruleAction : ( ( rule__Action__Group__0 ) ) ;
    public final void ruleAction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:166:2: ( ( ( rule__Action__Group__0 ) ) )
            // InternalWorkflow.g:167:2: ( ( rule__Action__Group__0 ) )
            {
            // InternalWorkflow.g:167:2: ( ( rule__Action__Group__0 ) )
            // InternalWorkflow.g:168:3: ( rule__Action__Group__0 )
            {
             before(grammarAccess.getActionAccess().getGroup()); 
            // InternalWorkflow.g:169:3: ( rule__Action__Group__0 )
            // InternalWorkflow.g:169:4: rule__Action__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "rule__StateMachine__Group__0"
    // InternalWorkflow.g:177:1: rule__StateMachine__Group__0 : rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 ;
    public final void rule__StateMachine__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:181:1: ( rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 )
            // InternalWorkflow.g:182:2: rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__StateMachine__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0"


    // $ANTLR start "rule__StateMachine__Group__0__Impl"
    // InternalWorkflow.g:189:1: rule__StateMachine__Group__0__Impl : ( ( rule__StateMachine__Group_0__0 ) ) ;
    public final void rule__StateMachine__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:193:1: ( ( ( rule__StateMachine__Group_0__0 ) ) )
            // InternalWorkflow.g:194:1: ( ( rule__StateMachine__Group_0__0 ) )
            {
            // InternalWorkflow.g:194:1: ( ( rule__StateMachine__Group_0__0 ) )
            // InternalWorkflow.g:195:2: ( rule__StateMachine__Group_0__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup_0()); 
            // InternalWorkflow.g:196:2: ( rule__StateMachine__Group_0__0 )
            // InternalWorkflow.g:196:3: rule__StateMachine__Group_0__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_0__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0__Impl"


    // $ANTLR start "rule__StateMachine__Group__1"
    // InternalWorkflow.g:204:1: rule__StateMachine__Group__1 : rule__StateMachine__Group__1__Impl ;
    public final void rule__StateMachine__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:208:1: ( rule__StateMachine__Group__1__Impl )
            // InternalWorkflow.g:209:2: rule__StateMachine__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1"


    // $ANTLR start "rule__StateMachine__Group__1__Impl"
    // InternalWorkflow.g:215:1: rule__StateMachine__Group__1__Impl : ( ( rule__StateMachine__Group_1__0 ) ) ;
    public final void rule__StateMachine__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:219:1: ( ( ( rule__StateMachine__Group_1__0 ) ) )
            // InternalWorkflow.g:220:1: ( ( rule__StateMachine__Group_1__0 ) )
            {
            // InternalWorkflow.g:220:1: ( ( rule__StateMachine__Group_1__0 ) )
            // InternalWorkflow.g:221:2: ( rule__StateMachine__Group_1__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup_1()); 
            // InternalWorkflow.g:222:2: ( rule__StateMachine__Group_1__0 )
            // InternalWorkflow.g:222:3: rule__StateMachine__Group_1__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1__Impl"


    // $ANTLR start "rule__StateMachine__Group_0__0"
    // InternalWorkflow.g:231:1: rule__StateMachine__Group_0__0 : rule__StateMachine__Group_0__0__Impl rule__StateMachine__Group_0__1 ;
    public final void rule__StateMachine__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:235:1: ( rule__StateMachine__Group_0__0__Impl rule__StateMachine__Group_0__1 )
            // InternalWorkflow.g:236:2: rule__StateMachine__Group_0__0__Impl rule__StateMachine__Group_0__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_0__0"


    // $ANTLR start "rule__StateMachine__Group_0__0__Impl"
    // InternalWorkflow.g:243:1: rule__StateMachine__Group_0__0__Impl : ( 'process:' ) ;
    public final void rule__StateMachine__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:247:1: ( ( 'process:' ) )
            // InternalWorkflow.g:248:1: ( 'process:' )
            {
            // InternalWorkflow.g:248:1: ( 'process:' )
            // InternalWorkflow.g:249:2: 'process:'
            {
             before(grammarAccess.getStateMachineAccess().getProcessKeyword_0_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getProcessKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_0__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_0__1"
    // InternalWorkflow.g:258:1: rule__StateMachine__Group_0__1 : rule__StateMachine__Group_0__1__Impl ;
    public final void rule__StateMachine__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:262:1: ( rule__StateMachine__Group_0__1__Impl )
            // InternalWorkflow.g:263:2: rule__StateMachine__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_0__1"


    // $ANTLR start "rule__StateMachine__Group_0__1__Impl"
    // InternalWorkflow.g:269:1: rule__StateMachine__Group_0__1__Impl : ( ( rule__StateMachine__NameAssignment_0_1 ) ) ;
    public final void rule__StateMachine__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:273:1: ( ( ( rule__StateMachine__NameAssignment_0_1 ) ) )
            // InternalWorkflow.g:274:1: ( ( rule__StateMachine__NameAssignment_0_1 ) )
            {
            // InternalWorkflow.g:274:1: ( ( rule__StateMachine__NameAssignment_0_1 ) )
            // InternalWorkflow.g:275:2: ( rule__StateMachine__NameAssignment_0_1 )
            {
             before(grammarAccess.getStateMachineAccess().getNameAssignment_0_1()); 
            // InternalWorkflow.g:276:2: ( rule__StateMachine__NameAssignment_0_1 )
            // InternalWorkflow.g:276:3: rule__StateMachine__NameAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__NameAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getNameAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_0__1__Impl"


    // $ANTLR start "rule__StateMachine__Group_1__0"
    // InternalWorkflow.g:285:1: rule__StateMachine__Group_1__0 : rule__StateMachine__Group_1__0__Impl rule__StateMachine__Group_1__1 ;
    public final void rule__StateMachine__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:289:1: ( rule__StateMachine__Group_1__0__Impl rule__StateMachine__Group_1__1 )
            // InternalWorkflow.g:290:2: rule__StateMachine__Group_1__0__Impl rule__StateMachine__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__0"


    // $ANTLR start "rule__StateMachine__Group_1__0__Impl"
    // InternalWorkflow.g:297:1: rule__StateMachine__Group_1__0__Impl : ( 'model:' ) ;
    public final void rule__StateMachine__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:301:1: ( ( 'model:' ) )
            // InternalWorkflow.g:302:1: ( 'model:' )
            {
            // InternalWorkflow.g:302:1: ( 'model:' )
            // InternalWorkflow.g:303:2: 'model:'
            {
             before(grammarAccess.getStateMachineAccess().getModelKeyword_1_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getModelKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_1__1"
    // InternalWorkflow.g:312:1: rule__StateMachine__Group_1__1 : rule__StateMachine__Group_1__1__Impl rule__StateMachine__Group_1__2 ;
    public final void rule__StateMachine__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:316:1: ( rule__StateMachine__Group_1__1__Impl rule__StateMachine__Group_1__2 )
            // InternalWorkflow.g:317:2: rule__StateMachine__Group_1__1__Impl rule__StateMachine__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__1"


    // $ANTLR start "rule__StateMachine__Group_1__1__Impl"
    // InternalWorkflow.g:324:1: rule__StateMachine__Group_1__1__Impl : ( ( rule__StateMachine__Group_1_1__0 )? ) ;
    public final void rule__StateMachine__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:328:1: ( ( ( rule__StateMachine__Group_1_1__0 )? ) )
            // InternalWorkflow.g:329:1: ( ( rule__StateMachine__Group_1_1__0 )? )
            {
            // InternalWorkflow.g:329:1: ( ( rule__StateMachine__Group_1_1__0 )? )
            // InternalWorkflow.g:330:2: ( rule__StateMachine__Group_1_1__0 )?
            {
             before(grammarAccess.getStateMachineAccess().getGroup_1_1()); 
            // InternalWorkflow.g:331:2: ( rule__StateMachine__Group_1_1__0 )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_ID) ) {
                    int LA1_2 = input.LA(3);

                    if ( (LA1_2==13) ) {
                        alt1=1;
                    }
                }
            }
            switch (alt1) {
                case 1 :
                    // InternalWorkflow.g:331:3: rule__StateMachine__Group_1_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__Group_1_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStateMachineAccess().getGroup_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__1__Impl"


    // $ANTLR start "rule__StateMachine__Group_1__2"
    // InternalWorkflow.g:339:1: rule__StateMachine__Group_1__2 : rule__StateMachine__Group_1__2__Impl ;
    public final void rule__StateMachine__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:343:1: ( rule__StateMachine__Group_1__2__Impl )
            // InternalWorkflow.g:344:2: rule__StateMachine__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__2"


    // $ANTLR start "rule__StateMachine__Group_1__2__Impl"
    // InternalWorkflow.g:350:1: rule__StateMachine__Group_1__2__Impl : ( ( ( rule__StateMachine__EndStatesAssignment_1_2 ) ) ( ( rule__StateMachine__EndStatesAssignment_1_2 )* ) ) ;
    public final void rule__StateMachine__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:354:1: ( ( ( ( rule__StateMachine__EndStatesAssignment_1_2 ) ) ( ( rule__StateMachine__EndStatesAssignment_1_2 )* ) ) )
            // InternalWorkflow.g:355:1: ( ( ( rule__StateMachine__EndStatesAssignment_1_2 ) ) ( ( rule__StateMachine__EndStatesAssignment_1_2 )* ) )
            {
            // InternalWorkflow.g:355:1: ( ( ( rule__StateMachine__EndStatesAssignment_1_2 ) ) ( ( rule__StateMachine__EndStatesAssignment_1_2 )* ) )
            // InternalWorkflow.g:356:2: ( ( rule__StateMachine__EndStatesAssignment_1_2 ) ) ( ( rule__StateMachine__EndStatesAssignment_1_2 )* )
            {
            // InternalWorkflow.g:356:2: ( ( rule__StateMachine__EndStatesAssignment_1_2 ) )
            // InternalWorkflow.g:357:3: ( rule__StateMachine__EndStatesAssignment_1_2 )
            {
             before(grammarAccess.getStateMachineAccess().getEndStatesAssignment_1_2()); 
            // InternalWorkflow.g:358:3: ( rule__StateMachine__EndStatesAssignment_1_2 )
            // InternalWorkflow.g:358:4: rule__StateMachine__EndStatesAssignment_1_2
            {
            pushFollow(FOLLOW_5);
            rule__StateMachine__EndStatesAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getEndStatesAssignment_1_2()); 

            }

            // InternalWorkflow.g:361:2: ( ( rule__StateMachine__EndStatesAssignment_1_2 )* )
            // InternalWorkflow.g:362:3: ( rule__StateMachine__EndStatesAssignment_1_2 )*
            {
             before(grammarAccess.getStateMachineAccess().getEndStatesAssignment_1_2()); 
            // InternalWorkflow.g:363:3: ( rule__StateMachine__EndStatesAssignment_1_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalWorkflow.g:363:4: rule__StateMachine__EndStatesAssignment_1_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__StateMachine__EndStatesAssignment_1_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getEndStatesAssignment_1_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1__2__Impl"


    // $ANTLR start "rule__StateMachine__Group_1_1__0"
    // InternalWorkflow.g:373:1: rule__StateMachine__Group_1_1__0 : rule__StateMachine__Group_1_1__0__Impl rule__StateMachine__Group_1_1__1 ;
    public final void rule__StateMachine__Group_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:377:1: ( rule__StateMachine__Group_1_1__0__Impl rule__StateMachine__Group_1_1__1 )
            // InternalWorkflow.g:378:2: rule__StateMachine__Group_1_1__0__Impl rule__StateMachine__Group_1_1__1
            {
            pushFollow(FOLLOW_4);
            rule__StateMachine__Group_1_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1_1__0"


    // $ANTLR start "rule__StateMachine__Group_1_1__0__Impl"
    // InternalWorkflow.g:385:1: rule__StateMachine__Group_1_1__0__Impl : ( ( rule__StateMachine__StartStateAssignment_1_1_0 ) ) ;
    public final void rule__StateMachine__Group_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:389:1: ( ( ( rule__StateMachine__StartStateAssignment_1_1_0 ) ) )
            // InternalWorkflow.g:390:1: ( ( rule__StateMachine__StartStateAssignment_1_1_0 ) )
            {
            // InternalWorkflow.g:390:1: ( ( rule__StateMachine__StartStateAssignment_1_1_0 ) )
            // InternalWorkflow.g:391:2: ( rule__StateMachine__StartStateAssignment_1_1_0 )
            {
             before(grammarAccess.getStateMachineAccess().getStartStateAssignment_1_1_0()); 
            // InternalWorkflow.g:392:2: ( rule__StateMachine__StartStateAssignment_1_1_0 )
            // InternalWorkflow.g:392:3: rule__StateMachine__StartStateAssignment_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__StartStateAssignment_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getStartStateAssignment_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1_1__0__Impl"


    // $ANTLR start "rule__StateMachine__Group_1_1__1"
    // InternalWorkflow.g:400:1: rule__StateMachine__Group_1_1__1 : rule__StateMachine__Group_1_1__1__Impl ;
    public final void rule__StateMachine__Group_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:404:1: ( rule__StateMachine__Group_1_1__1__Impl )
            // InternalWorkflow.g:405:2: rule__StateMachine__Group_1_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group_1_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1_1__1"


    // $ANTLR start "rule__StateMachine__Group_1_1__1__Impl"
    // InternalWorkflow.g:411:1: rule__StateMachine__Group_1_1__1__Impl : ( ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )* ) ;
    public final void rule__StateMachine__Group_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:415:1: ( ( ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )* ) )
            // InternalWorkflow.g:416:1: ( ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )* )
            {
            // InternalWorkflow.g:416:1: ( ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )* )
            // InternalWorkflow.g:417:2: ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )*
            {
             before(grammarAccess.getStateMachineAccess().getIntermediateStatesAssignment_1_1_1()); 
            // InternalWorkflow.g:418:2: ( rule__StateMachine__IntermediateStatesAssignment_1_1_1 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    int LA3_1 = input.LA(2);

                    if ( (LA3_1==RULE_ID) ) {
                        int LA3_2 = input.LA(3);

                        if ( (LA3_2==13) ) {
                            alt3=1;
                        }


                    }


                }


                switch (alt3) {
            	case 1 :
            	    // InternalWorkflow.g:418:3: rule__StateMachine__IntermediateStatesAssignment_1_1_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__StateMachine__IntermediateStatesAssignment_1_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getIntermediateStatesAssignment_1_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group_1_1__1__Impl"


    // $ANTLR start "rule__StartState__Group__0"
    // InternalWorkflow.g:427:1: rule__StartState__Group__0 : rule__StartState__Group__0__Impl rule__StartState__Group__1 ;
    public final void rule__StartState__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:431:1: ( rule__StartState__Group__0__Impl rule__StartState__Group__1 )
            // InternalWorkflow.g:432:2: rule__StartState__Group__0__Impl rule__StartState__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__StartState__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StartState__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__Group__0"


    // $ANTLR start "rule__StartState__Group__0__Impl"
    // InternalWorkflow.g:439:1: rule__StartState__Group__0__Impl : ( ( rule__StartState__NameAssignment_0 ) ) ;
    public final void rule__StartState__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:443:1: ( ( ( rule__StartState__NameAssignment_0 ) ) )
            // InternalWorkflow.g:444:1: ( ( rule__StartState__NameAssignment_0 ) )
            {
            // InternalWorkflow.g:444:1: ( ( rule__StartState__NameAssignment_0 ) )
            // InternalWorkflow.g:445:2: ( rule__StartState__NameAssignment_0 )
            {
             before(grammarAccess.getStartStateAccess().getNameAssignment_0()); 
            // InternalWorkflow.g:446:2: ( rule__StartState__NameAssignment_0 )
            // InternalWorkflow.g:446:3: rule__StartState__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StartState__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStartStateAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__Group__0__Impl"


    // $ANTLR start "rule__StartState__Group__1"
    // InternalWorkflow.g:454:1: rule__StartState__Group__1 : rule__StartState__Group__1__Impl ;
    public final void rule__StartState__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:458:1: ( rule__StartState__Group__1__Impl )
            // InternalWorkflow.g:459:2: rule__StartState__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StartState__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__Group__1"


    // $ANTLR start "rule__StartState__Group__1__Impl"
    // InternalWorkflow.g:465:1: rule__StartState__Group__1__Impl : ( ( ( rule__StartState__ActionsAssignment_1 ) ) ( ( rule__StartState__ActionsAssignment_1 )* ) ) ;
    public final void rule__StartState__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:469:1: ( ( ( ( rule__StartState__ActionsAssignment_1 ) ) ( ( rule__StartState__ActionsAssignment_1 )* ) ) )
            // InternalWorkflow.g:470:1: ( ( ( rule__StartState__ActionsAssignment_1 ) ) ( ( rule__StartState__ActionsAssignment_1 )* ) )
            {
            // InternalWorkflow.g:470:1: ( ( ( rule__StartState__ActionsAssignment_1 ) ) ( ( rule__StartState__ActionsAssignment_1 )* ) )
            // InternalWorkflow.g:471:2: ( ( rule__StartState__ActionsAssignment_1 ) ) ( ( rule__StartState__ActionsAssignment_1 )* )
            {
            // InternalWorkflow.g:471:2: ( ( rule__StartState__ActionsAssignment_1 ) )
            // InternalWorkflow.g:472:3: ( rule__StartState__ActionsAssignment_1 )
            {
             before(grammarAccess.getStartStateAccess().getActionsAssignment_1()); 
            // InternalWorkflow.g:473:3: ( rule__StartState__ActionsAssignment_1 )
            // InternalWorkflow.g:473:4: rule__StartState__ActionsAssignment_1
            {
            pushFollow(FOLLOW_5);
            rule__StartState__ActionsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStartStateAccess().getActionsAssignment_1()); 

            }

            // InternalWorkflow.g:476:2: ( ( rule__StartState__ActionsAssignment_1 )* )
            // InternalWorkflow.g:477:3: ( rule__StartState__ActionsAssignment_1 )*
            {
             before(grammarAccess.getStartStateAccess().getActionsAssignment_1()); 
            // InternalWorkflow.g:478:3: ( rule__StartState__ActionsAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    int LA4_2 = input.LA(2);

                    if ( (LA4_2==13) ) {
                        alt4=1;
                    }


                }


                switch (alt4) {
            	case 1 :
            	    // InternalWorkflow.g:478:4: rule__StartState__ActionsAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__StartState__ActionsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getStartStateAccess().getActionsAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__Group__1__Impl"


    // $ANTLR start "rule__IntermediateState__Group__0"
    // InternalWorkflow.g:488:1: rule__IntermediateState__Group__0 : rule__IntermediateState__Group__0__Impl rule__IntermediateState__Group__1 ;
    public final void rule__IntermediateState__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:492:1: ( rule__IntermediateState__Group__0__Impl rule__IntermediateState__Group__1 )
            // InternalWorkflow.g:493:2: rule__IntermediateState__Group__0__Impl rule__IntermediateState__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__IntermediateState__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntermediateState__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__Group__0"


    // $ANTLR start "rule__IntermediateState__Group__0__Impl"
    // InternalWorkflow.g:500:1: rule__IntermediateState__Group__0__Impl : ( ( rule__IntermediateState__NameAssignment_0 ) ) ;
    public final void rule__IntermediateState__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:504:1: ( ( ( rule__IntermediateState__NameAssignment_0 ) ) )
            // InternalWorkflow.g:505:1: ( ( rule__IntermediateState__NameAssignment_0 ) )
            {
            // InternalWorkflow.g:505:1: ( ( rule__IntermediateState__NameAssignment_0 ) )
            // InternalWorkflow.g:506:2: ( rule__IntermediateState__NameAssignment_0 )
            {
             before(grammarAccess.getIntermediateStateAccess().getNameAssignment_0()); 
            // InternalWorkflow.g:507:2: ( rule__IntermediateState__NameAssignment_0 )
            // InternalWorkflow.g:507:3: rule__IntermediateState__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__IntermediateState__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getIntermediateStateAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__Group__0__Impl"


    // $ANTLR start "rule__IntermediateState__Group__1"
    // InternalWorkflow.g:515:1: rule__IntermediateState__Group__1 : rule__IntermediateState__Group__1__Impl ;
    public final void rule__IntermediateState__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:519:1: ( rule__IntermediateState__Group__1__Impl )
            // InternalWorkflow.g:520:2: rule__IntermediateState__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntermediateState__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__Group__1"


    // $ANTLR start "rule__IntermediateState__Group__1__Impl"
    // InternalWorkflow.g:526:1: rule__IntermediateState__Group__1__Impl : ( ( ( rule__IntermediateState__ActionsAssignment_1 ) ) ( ( rule__IntermediateState__ActionsAssignment_1 )* ) ) ;
    public final void rule__IntermediateState__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:530:1: ( ( ( ( rule__IntermediateState__ActionsAssignment_1 ) ) ( ( rule__IntermediateState__ActionsAssignment_1 )* ) ) )
            // InternalWorkflow.g:531:1: ( ( ( rule__IntermediateState__ActionsAssignment_1 ) ) ( ( rule__IntermediateState__ActionsAssignment_1 )* ) )
            {
            // InternalWorkflow.g:531:1: ( ( ( rule__IntermediateState__ActionsAssignment_1 ) ) ( ( rule__IntermediateState__ActionsAssignment_1 )* ) )
            // InternalWorkflow.g:532:2: ( ( rule__IntermediateState__ActionsAssignment_1 ) ) ( ( rule__IntermediateState__ActionsAssignment_1 )* )
            {
            // InternalWorkflow.g:532:2: ( ( rule__IntermediateState__ActionsAssignment_1 ) )
            // InternalWorkflow.g:533:3: ( rule__IntermediateState__ActionsAssignment_1 )
            {
             before(grammarAccess.getIntermediateStateAccess().getActionsAssignment_1()); 
            // InternalWorkflow.g:534:3: ( rule__IntermediateState__ActionsAssignment_1 )
            // InternalWorkflow.g:534:4: rule__IntermediateState__ActionsAssignment_1
            {
            pushFollow(FOLLOW_5);
            rule__IntermediateState__ActionsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIntermediateStateAccess().getActionsAssignment_1()); 

            }

            // InternalWorkflow.g:537:2: ( ( rule__IntermediateState__ActionsAssignment_1 )* )
            // InternalWorkflow.g:538:3: ( rule__IntermediateState__ActionsAssignment_1 )*
            {
             before(grammarAccess.getIntermediateStateAccess().getActionsAssignment_1()); 
            // InternalWorkflow.g:539:3: ( rule__IntermediateState__ActionsAssignment_1 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    int LA5_2 = input.LA(2);

                    if ( (LA5_2==13) ) {
                        alt5=1;
                    }


                }


                switch (alt5) {
            	case 1 :
            	    // InternalWorkflow.g:539:4: rule__IntermediateState__ActionsAssignment_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__IntermediateState__ActionsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getIntermediateStateAccess().getActionsAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__Group__1__Impl"


    // $ANTLR start "rule__Action__Group__0"
    // InternalWorkflow.g:549:1: rule__Action__Group__0 : rule__Action__Group__0__Impl rule__Action__Group__1 ;
    public final void rule__Action__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:553:1: ( rule__Action__Group__0__Impl rule__Action__Group__1 )
            // InternalWorkflow.g:554:2: rule__Action__Group__0__Impl rule__Action__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Action__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0"


    // $ANTLR start "rule__Action__Group__0__Impl"
    // InternalWorkflow.g:561:1: rule__Action__Group__0__Impl : ( ( rule__Action__NameAssignment_0 ) ) ;
    public final void rule__Action__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:565:1: ( ( ( rule__Action__NameAssignment_0 ) ) )
            // InternalWorkflow.g:566:1: ( ( rule__Action__NameAssignment_0 ) )
            {
            // InternalWorkflow.g:566:1: ( ( rule__Action__NameAssignment_0 ) )
            // InternalWorkflow.g:567:2: ( rule__Action__NameAssignment_0 )
            {
             before(grammarAccess.getActionAccess().getNameAssignment_0()); 
            // InternalWorkflow.g:568:2: ( rule__Action__NameAssignment_0 )
            // InternalWorkflow.g:568:3: rule__Action__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Action__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0__Impl"


    // $ANTLR start "rule__Action__Group__1"
    // InternalWorkflow.g:576:1: rule__Action__Group__1 : rule__Action__Group__1__Impl rule__Action__Group__2 ;
    public final void rule__Action__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:580:1: ( rule__Action__Group__1__Impl rule__Action__Group__2 )
            // InternalWorkflow.g:581:2: rule__Action__Group__1__Impl rule__Action__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Action__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1"


    // $ANTLR start "rule__Action__Group__1__Impl"
    // InternalWorkflow.g:588:1: rule__Action__Group__1__Impl : ( '=>' ) ;
    public final void rule__Action__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:592:1: ( ( '=>' ) )
            // InternalWorkflow.g:593:1: ( '=>' )
            {
            // InternalWorkflow.g:593:1: ( '=>' )
            // InternalWorkflow.g:594:2: '=>'
            {
             before(grammarAccess.getActionAccess().getEqualsSignGreaterThanSignKeyword_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getEqualsSignGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1__Impl"


    // $ANTLR start "rule__Action__Group__2"
    // InternalWorkflow.g:603:1: rule__Action__Group__2 : rule__Action__Group__2__Impl ;
    public final void rule__Action__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:607:1: ( rule__Action__Group__2__Impl )
            // InternalWorkflow.g:608:2: rule__Action__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2"


    // $ANTLR start "rule__Action__Group__2__Impl"
    // InternalWorkflow.g:614:1: rule__Action__Group__2__Impl : ( ( rule__Action__DestinationStateAssignment_2 ) ) ;
    public final void rule__Action__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:618:1: ( ( ( rule__Action__DestinationStateAssignment_2 ) ) )
            // InternalWorkflow.g:619:1: ( ( rule__Action__DestinationStateAssignment_2 ) )
            {
            // InternalWorkflow.g:619:1: ( ( rule__Action__DestinationStateAssignment_2 ) )
            // InternalWorkflow.g:620:2: ( rule__Action__DestinationStateAssignment_2 )
            {
             before(grammarAccess.getActionAccess().getDestinationStateAssignment_2()); 
            // InternalWorkflow.g:621:2: ( rule__Action__DestinationStateAssignment_2 )
            // InternalWorkflow.g:621:3: rule__Action__DestinationStateAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Action__DestinationStateAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getDestinationStateAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2__Impl"


    // $ANTLR start "rule__StateMachine__NameAssignment_0_1"
    // InternalWorkflow.g:630:1: rule__StateMachine__NameAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__StateMachine__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:634:1: ( ( RULE_ID ) )
            // InternalWorkflow.g:635:2: ( RULE_ID )
            {
            // InternalWorkflow.g:635:2: ( RULE_ID )
            // InternalWorkflow.g:636:3: RULE_ID
            {
             before(grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getNameIDTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__NameAssignment_0_1"


    // $ANTLR start "rule__StateMachine__StartStateAssignment_1_1_0"
    // InternalWorkflow.g:645:1: rule__StateMachine__StartStateAssignment_1_1_0 : ( ruleStartState ) ;
    public final void rule__StateMachine__StartStateAssignment_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:649:1: ( ( ruleStartState ) )
            // InternalWorkflow.g:650:2: ( ruleStartState )
            {
            // InternalWorkflow.g:650:2: ( ruleStartState )
            // InternalWorkflow.g:651:3: ruleStartState
            {
             before(grammarAccess.getStateMachineAccess().getStartStateStartStateParserRuleCall_1_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleStartState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStartStateStartStateParserRuleCall_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StartStateAssignment_1_1_0"


    // $ANTLR start "rule__StateMachine__IntermediateStatesAssignment_1_1_1"
    // InternalWorkflow.g:660:1: rule__StateMachine__IntermediateStatesAssignment_1_1_1 : ( ruleIntermediateState ) ;
    public final void rule__StateMachine__IntermediateStatesAssignment_1_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:664:1: ( ( ruleIntermediateState ) )
            // InternalWorkflow.g:665:2: ( ruleIntermediateState )
            {
            // InternalWorkflow.g:665:2: ( ruleIntermediateState )
            // InternalWorkflow.g:666:3: ruleIntermediateState
            {
             before(grammarAccess.getStateMachineAccess().getIntermediateStatesIntermediateStateParserRuleCall_1_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIntermediateState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getIntermediateStatesIntermediateStateParserRuleCall_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__IntermediateStatesAssignment_1_1_1"


    // $ANTLR start "rule__StateMachine__EndStatesAssignment_1_2"
    // InternalWorkflow.g:675:1: rule__StateMachine__EndStatesAssignment_1_2 : ( ruleEndState ) ;
    public final void rule__StateMachine__EndStatesAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:679:1: ( ( ruleEndState ) )
            // InternalWorkflow.g:680:2: ( ruleEndState )
            {
            // InternalWorkflow.g:680:2: ( ruleEndState )
            // InternalWorkflow.g:681:3: ruleEndState
            {
             before(grammarAccess.getStateMachineAccess().getEndStatesEndStateParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEndState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getEndStatesEndStateParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__EndStatesAssignment_1_2"


    // $ANTLR start "rule__StartState__NameAssignment_0"
    // InternalWorkflow.g:690:1: rule__StartState__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__StartState__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:694:1: ( ( RULE_ID ) )
            // InternalWorkflow.g:695:2: ( RULE_ID )
            {
            // InternalWorkflow.g:695:2: ( RULE_ID )
            // InternalWorkflow.g:696:3: RULE_ID
            {
             before(grammarAccess.getStartStateAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStartStateAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__NameAssignment_0"


    // $ANTLR start "rule__StartState__ActionsAssignment_1"
    // InternalWorkflow.g:705:1: rule__StartState__ActionsAssignment_1 : ( ruleAction ) ;
    public final void rule__StartState__ActionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:709:1: ( ( ruleAction ) )
            // InternalWorkflow.g:710:2: ( ruleAction )
            {
            // InternalWorkflow.g:710:2: ( ruleAction )
            // InternalWorkflow.g:711:3: ruleAction
            {
             before(grammarAccess.getStartStateAccess().getActionsActionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getStartStateAccess().getActionsActionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StartState__ActionsAssignment_1"


    // $ANTLR start "rule__IntermediateState__NameAssignment_0"
    // InternalWorkflow.g:720:1: rule__IntermediateState__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__IntermediateState__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:724:1: ( ( RULE_ID ) )
            // InternalWorkflow.g:725:2: ( RULE_ID )
            {
            // InternalWorkflow.g:725:2: ( RULE_ID )
            // InternalWorkflow.g:726:3: RULE_ID
            {
             before(grammarAccess.getIntermediateStateAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getIntermediateStateAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__NameAssignment_0"


    // $ANTLR start "rule__IntermediateState__ActionsAssignment_1"
    // InternalWorkflow.g:735:1: rule__IntermediateState__ActionsAssignment_1 : ( ruleAction ) ;
    public final void rule__IntermediateState__ActionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:739:1: ( ( ruleAction ) )
            // InternalWorkflow.g:740:2: ( ruleAction )
            {
            // InternalWorkflow.g:740:2: ( ruleAction )
            // InternalWorkflow.g:741:3: ruleAction
            {
             before(grammarAccess.getIntermediateStateAccess().getActionsActionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getIntermediateStateAccess().getActionsActionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntermediateState__ActionsAssignment_1"


    // $ANTLR start "rule__EndState__NameAssignment"
    // InternalWorkflow.g:750:1: rule__EndState__NameAssignment : ( RULE_ID ) ;
    public final void rule__EndState__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:754:1: ( ( RULE_ID ) )
            // InternalWorkflow.g:755:2: ( RULE_ID )
            {
            // InternalWorkflow.g:755:2: ( RULE_ID )
            // InternalWorkflow.g:756:3: RULE_ID
            {
             before(grammarAccess.getEndStateAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEndStateAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EndState__NameAssignment"


    // $ANTLR start "rule__Action__NameAssignment_0"
    // InternalWorkflow.g:765:1: rule__Action__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Action__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:769:1: ( ( RULE_ID ) )
            // InternalWorkflow.g:770:2: ( RULE_ID )
            {
            // InternalWorkflow.g:770:2: ( RULE_ID )
            // InternalWorkflow.g:771:3: RULE_ID
            {
             before(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__NameAssignment_0"


    // $ANTLR start "rule__Action__DestinationStateAssignment_2"
    // InternalWorkflow.g:780:1: rule__Action__DestinationStateAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Action__DestinationStateAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWorkflow.g:784:1: ( ( ( RULE_ID ) ) )
            // InternalWorkflow.g:785:2: ( ( RULE_ID ) )
            {
            // InternalWorkflow.g:785:2: ( ( RULE_ID ) )
            // InternalWorkflow.g:786:3: ( RULE_ID )
            {
             before(grammarAccess.getActionAccess().getDestinationStateDestinationStateCrossReference_2_0()); 
            // InternalWorkflow.g:787:3: ( RULE_ID )
            // InternalWorkflow.g:788:4: RULE_ID
            {
             before(grammarAccess.getActionAccess().getDestinationStateDestinationStateIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getDestinationStateDestinationStateIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getActionAccess().getDestinationStateDestinationStateCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__DestinationStateAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});

}