# DSL: Banking workflow

## Example

I thought of the possibly simplest way of representing a finite
state machine with some special restrictions as follows:

```
process: EndToEndPaymentProcessing

model:
    Initialization
        prechecks => Validation
    Validation
        suspend => Suspended
        redo => Validation
        approve => Approved
        decline => Declined
    Suspended
        revalidate => Validation
    Declined
    Approved
```

The example shows that the whole model is defined in terms of states and the transitions you can make from these.

## Design decisions and comments

This design has the positive properties of not duplicating any information
and has spatially local definitions of actions next to corresponding states
these can be made on.

The design also has the aspect of having the start state declared first,
intermediate states afterwards, and end states last, ultimately enforcing
to never miss defining a start state or an end state.

I put no special restrictions on the naming of the states and actions here casing-wise, as it gives users freedom to spell identifiers their preferred way (e.g. in SNAKE_CASE for states).

The decision to use `=>` (instead of e.g. nothing at all) was for
the sake of better readability.

I decided in favor of explicit declarations of `process` and `model` (instead of e.g. nothing at all) in order to avoid confusion in a situation where an user misleadingly declares his start state as the process identifier instead (which may happen in the situation where the start state is also the end state).

The DSL file extension is _abswf_, standing for Avaloq Banking System Workflow.

## Details

Files worth checking (not solely generated):
* abs.workflow/src/abs/Workflow.xtext
* abs.workflow/src/abs/GenerateWorkflow.mwe2
* abs.workflow/src/abs/validation/WorkflowValidator.xtend
* abs.workflow.tests/src/abs/tests/WorkflowParsingTest.xtend
* sample.abswf

The solution follows all the points:
>The main elements of the language are:
* identifier of the business process (one needs to know whether he is describing a payment, stex or account opening workflow) being described
* states
* actions
* States are connected via actions, e.g. state --> action --> state
* An action transitions an order between two states in one direction
* An actions target and source state can be identical
* Each state has an identifier
* Each action has an identifier
Optional:
* a workflow should have a unique start state:
    - the identifier of the start state is not suitable to indicate whether it is a start state or not, since it can be chosen by the business analyst developing the workflow
    - a start state can only have out-going actions
* a workflow can have more than one end state:
    - the identifiers of the end states are not suitable to indicate whether they are end states (same reasoning as above)
    - an end state can only have incoming actions (if an order is in an end state it the business process is done)
* scoping, referencing, checks, etc. e.g. identifiers should be unique

## Future improvements

* More descriptive error messages
* Better scoping of errors (e.g. underlining only the invalid part)
* More thorough testing, especially w.r.t. what is highlighted and in which situations
* Adjust model to take care of current manual validation of whether non-start states are reachable OR improve the current checks in such a way that the validation through the state machine happens only once

